package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"github.com/satori/go.uuid"
)

type ClientManager struct {
	clients    map[*Client]bool
	broadcast  chan []byte
	register   chan *Client
	unregister chan *Client
}

type Client struct {
	id     string
	socket *websocket.Conn
	send   chan []byte
}

type Message struct {
	Sender    string `json:"sender,omitempty"`
	Recipient string `json:"recipient,omitempty"`
	Content   string `json:"content,omitempty"`
}

type Orders struct {
	Type string  `json:"type"`
	Data []Order `json:"data"`
}

type Order struct {
	Price float32 `json:"price"`
	Size  float32 `json:"size"`
	Total float32 `json:"total"`
	Type  string  `json:"type"`
}

type OpenOrders struct {
	Type string
	Data []OpenOrder
}

type OpenOrder struct {
	MinPrcScale int
	Price       int
	MinQtyScale int
	OrdQty      int
	TradeQty    int
	Side        int
}

type TimeSaleData struct {
	Type string
	Data TimeSale
}

type TimeSale struct {
	Security  string
	Volumn    float64
	Price     float64
	Timestamp int64
}

var manager = ClientManager{
	broadcast:  make(chan []byte),
	register:   make(chan *Client),
	unregister: make(chan *Client),
	clients:    make(map[*Client]bool),
}

func (manager *ClientManager) start() {
	for {
		select {
		case conn := <-manager.register:
			manager.clients[conn] = true
			jsonMessage, _ := json.Marshal(&Message{Content: "/A new socket has connected."})
			manager.send(jsonMessage, conn)
		case conn := <-manager.unregister:
			if _, ok := manager.clients[conn]; ok {
				close(conn.send)
				delete(manager.clients, conn)
				jsonMessage, _ := json.Marshal(&Message{Content: "/A socket has disconnected."})
				manager.send(jsonMessage, conn)
			}
		case message := <-manager.broadcast:
			for conn := range manager.clients {
				select {
				case conn.send <- message:
				default:
					close(conn.send)
					delete(manager.clients, conn)
				}
			}
		}
	}
}

func (manager *ClientManager) send(message []byte, ignore *Client) {
	for conn := range manager.clients {
		if conn != ignore {
			conn.send <- message
		}
	}
}

func (c *Client) read() {
	defer func() {
		manager.unregister <- c
		c.socket.Close()
	}()

	for {
		_, message, err := c.socket.ReadMessage()
		if err != nil {
			manager.unregister <- c
			c.socket.Close()
			break
		}

		if string(message) == "mock_order" {
			seed := rand.NewSource(time.Now().UnixNano())
			rand := rand.New(seed)

			orders := Orders{Type: "mock_order", Data: []Order{}}

			for i := 0; i < 11; i++ {
				orders.Data = append(orders.Data, Order{
					Price: rand.Float32() + 5,
					Size:  rand.Float32() * 1000,
					Total: rand.Float32() * 1000,
					Type:  "SELL",
				})
			}

			for i := 0; i < 11; i++ {
				orders.Data = append(orders.Data, Order{
					Price: rand.Float32() + 5,
					Size:  rand.Float32() * 1000,
					Total: rand.Float32() * 1000,
					Type:  "BUY",
				})
			}

			jsonMessage, _ := json.Marshal(orders)
			manager.broadcast <- jsonMessage

		} else if string(message) == "mock_open_order" {
			// http://localhost:1234/mock.html
			seed := rand.NewSource(time.Now().UnixNano())
			rand := rand.New(seed)

			openOrders := OpenOrders{Type: "openorders", Data: []OpenOrder{}}

			randBase := 680000

			for i := 0; i < 11; i++ {
				openOrders.Data = append(openOrders.Data, OpenOrder{
					MinPrcScale: 2,
					Price:       randBase + rand.Intn(20000),
					MinQtyScale: 4,
					OrdQty:      rand.Intn(20000000),
					TradeQty:    rand.Intn(20000000),
					Side:        2,
				})
			}

			for i := 0; i < 11; i++ {
				openOrders.Data = append(openOrders.Data, OpenOrder{
					MinPrcScale: 2,
					Price:       randBase + rand.Intn(20000),
					MinQtyScale: 4,
					OrdQty:      rand.Intn(20000000),
					TradeQty:    rand.Intn(20000000),
					Side:        1,
				})
			}

			jsonMessage, _ := json.Marshal(openOrders)
			manager.broadcast <- jsonMessage
		} else if string(message) == "mock_timesale" {
			seed := rand.NewSource(time.Now().UnixNano())
			rand := rand.New(seed)

			timeSale := TimeSale{
				Security:  "BTC/USDT",
				Volumn:    rand.Float64() * 10000,
				Price:     6800 + rand.Float64()*100,
				Timestamp: time.Now().Unix(),
			}

			timeSaleData := TimeSaleData{
				Type: "timesale",
				Data: timeSale,
			}

			jsonMessage, _ := json.Marshal(timeSaleData)
			manager.broadcast <- jsonMessage
		} else {
			jsonMessage, _ := json.Marshal(&Message{Sender: c.id, Content: string(message)})
			manager.broadcast <- jsonMessage
		}

	}
}

func (c *Client) write() {
	defer func() {
		c.socket.Close()
	}()

	for {
		select {
		case message, ok := <-c.send:
			if !ok {
				c.socket.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			c.socket.WriteMessage(websocket.TextMessage, message)
			// c.socket.WriteMessage(websocket.TextMessage, []byte("test if this is what is going to be write"))
		}
	}
}

func main() {
	fmt.Println("Starting application...")
	go manager.start()

	fs := http.FileServer(http.Dir("public"))
	http.Handle("/", fs)

	http.HandleFunc("/ws", wsPage)
	http.ListenAndServe(":1234", nil)
}

func wsPage(res http.ResponseWriter, req *http.Request) {
	conn, error := (&websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}).Upgrade(res, req, nil)
	if error != nil {
		http.NotFound(res, req)
		return
	}

	u, _ := uuid.NewV4()

	client := &Client{id: u.String(), socket: conn, send: make(chan []byte)}

	manager.register <- client

	go client.read()
	go client.write()
}
